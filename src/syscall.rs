//
//    wayless, an operating system built in Rust
//    Copyright (C) Waylon Cude 2019
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, under version 2 of the License
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//


// This macro takes a list of syscalls and ties them to the corresponding
// seL4 syscalls, ensuring that our syscalls have the same numbering
// as seL4.
macro_rules! syscalls {
    ($($syscall:ident),+ $(,)*) => {
        //We use this mashup macro so we can concatenate stuff
        //concat_idents can't be used in pattern position
        mashup! {
            $(
                sys["sys" $syscall] = syscall_ $syscall;
             )+
        }

        #[derive(Debug,PartialEq)]
        pub enum Syscall {
            //Assign seL4's syscall number to each variant of Syscall
            $($syscall = sys!(crate::sel4::"sys"$syscall) as isize),+,
        }
        impl Syscall {
            //This function lets us convert from syscall number
            //to a Syscall
            pub fn from_i32(syscall_num: i32) -> Option<Syscall> {
                use self::Syscall::*;

                match syscall_num {
                    //Turn a syscall number from seL4 into a Syscall
                    $(
                        sys!(crate::sel4::"sys"$syscall) => 
                        Some($syscall)),+,
                    _ => None
                }
            }
        }
    }
}

syscalls!{
    SysSend,
    SysNBSend,
    SysCall,
    SysRecv,
    SysReply,
    SysReplyRecv,
    SysNBRecv,
    SysYield,
}

#[cfg(test)]
mod test {
    use std;
    use crate::sel4;
    use super::Syscall;
    use proptest::prelude::*;

    proptest!{
        //Check to see if syscall numbers that are in range give a valid
        //syscall
        #[test]
        fn in_range_test(syscall_num in sel4::SYSCALL_MIN ..= sel4::SYSCALL_MAX) {
            Syscall::from_i32(syscall_num).unwrap()
        }
        //Check to see if syscall is valid, if it's in range or not
        #[test]
        fn matches_test(syscall_num in any::<i32>()) {
            let syscall = Syscall::from_i32(syscall_num);
            if syscall_num >= sel4::SYSCALL_MIN && syscall_num <= sel4::SYSCALL_MAX {
                syscall.unwrap();
            } else {
                assert_eq!(syscall,None)
            }
        }
    }
}

